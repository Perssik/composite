package com.company.compartments;

import com.company.abstraction.iToolCompartment;
import com.company.tools.Tools;

public class SpannerCompartment implements iToolCompartment {

    Tools tool = Tools.SPANNER;

    @Override
    public void printToolCompartmentAssignment() {
        System.out.println("This compartment is for :" + tool);
    }
}
